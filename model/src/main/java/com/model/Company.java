package com.model;

public class Company {
	private String name = "";
	private String jmsRoute = "";
	
	
	public Company(String name, String jmsRoute) {
		this.jmsRoute = jmsRoute;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getJmsRoute() {
		return jmsRoute;
	}


	public void setJmsRoute(String jmsRoute) {
		this.jmsRoute = jmsRoute;
	}

}
