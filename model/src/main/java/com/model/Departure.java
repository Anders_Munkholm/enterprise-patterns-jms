package com.model;

import java.sql.Time;
import java.util.Date;

public class Departure {
	String airline;
	String flightNo;
	String destination;
	String origin;
	String arrivalDeparture;
	String date;
	
	public Departure() {
		
	}
	
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getArrivalDeparture() {
		return arrivalDeparture;
	}
	public void setArrivalDeparture(String arrivalDeparture) {
		this.arrivalDeparture = arrivalDeparture;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	

}
