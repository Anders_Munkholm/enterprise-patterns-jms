package com.model;

public class Airplane {
	int flightNumber;
	
	public Airplane(){
		
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}
	
}
