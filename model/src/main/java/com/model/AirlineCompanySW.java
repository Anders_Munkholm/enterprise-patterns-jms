package com.model;

import java.sql.Time;
import java.util.Date;

public class AirlineCompanySW {
	String airline;
	String flightNo;
	String destination;
	String date;
	String departure;

	
	public AirlineCompanySW(String airline, String flightNo, String destination, String date, String departure) {
		this.airline = airline;
		this.flightNo = flightNo;
		this.destination = destination;
		this.date = date;
		this.departure = departure;
	}


	public String getAirline() {
		return airline;
	}


	public void setAirline(String airline) {
		this.airline = airline;
	}


	public String getFlightNo() {
		return flightNo;
	}


	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getDeparture() {
		return departure;
	}


	public void setDeparture(String departure) {
		this.departure = departure;
	}
	
	
}
