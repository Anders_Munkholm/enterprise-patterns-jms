package com.model;

import java.sql.Time;
import java.util.Date;

public class AirlineCompanySAS {
	String airline;
	String flightNo;
	String destination;
	String origin;
	String arrivalDeparture;
	String dato;
	String tidspunkt;
	
	public AirlineCompanySAS(String airline, String flightNo, String destination, String origin,String arrivalDeparture, String dato, String tidspunkt) {
		this.airline = airline;
		this.flightNo = flightNo;
		this.destination = destination;
		this.arrivalDeparture = arrivalDeparture;;
		this.origin = origin;
		this.dato = dato;
		this.tidspunkt = tidspunkt;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getArrivalDeparture() {
		return arrivalDeparture;
	}

	public void setArrivalDeparture(String arrivalDeparture) {
		this.arrivalDeparture = arrivalDeparture;
	}

	public String getDato() {
		return dato;
	}

	public void setDato(String dato) {
		this.dato = dato;
	}

	public String getTidspunkt() {
		return tidspunkt;
	}

	public void setTidspunkt(String tidspunkt) {
		this.tidspunkt = tidspunkt;
	}

}
