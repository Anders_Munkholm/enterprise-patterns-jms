package com.model;

public class Message {

    private String head;
    private String body;

    public Message() {
    }

    public Message(String head, String body) {
        this.head = head;
        this.body = body;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    @Override
    public String toString() {
        return String.format("Message{head=%s, body=%s}", getHead(), getBody());
    }


	

}
