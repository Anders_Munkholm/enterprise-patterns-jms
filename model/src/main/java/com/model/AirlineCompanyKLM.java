package com.model;

import java.util.Date;

public class AirlineCompanyKLM {
	String airline;
	String flightNo;
	String destination;
	String origin;
	String date;

	
	public AirlineCompanyKLM(String airline, String flightNo, String destination, String origin, String date) {
		this.airline = airline;
		this.flightNo = flightNo;
		this.destination = destination;
		this.origin = origin;
		this.date = date;
	}


	public String getAirline() {
		return airline;
	}


	public void setAirline(String airline) {
		this.airline = airline;
	}


	public String getFlightNo() {
		return flightNo;
	}


	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getOrigin() {
		return origin;
	}


	public void setOrigin(String origin) {
		this.origin = origin;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}
	
	
}
