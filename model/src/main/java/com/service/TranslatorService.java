package com.service;

import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Departure;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
@Service
public class TranslatorService {

	@Autowired
	private JmsMessageService jmsMessageService;
	@Autowired
	private JmsMessageTemplateService jmsMessageTemplateService;
	/**
	 * 
	 * @param message
	 * @return Map the first parameter of the map is attributeName the second is the value
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public Map<String, Object> getMessageJsonMap(Message message) throws JsonParseException, JsonMappingException, IOException {
		String json = this.jmsMessageService.extractTextMessageString(message);
		ObjectMapper jsonMapper = new ObjectMapper();
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		jsonMap = jsonMapper.readValue(json, new TypeReference<Map<String, Object>>(){});
		return jsonMap;
	}



	public void sendTranslatedMessage(Departure departure) {
		jmsMessageTemplateService.getJmsTemplate().convertAndSend("TranslatedMessageReciever",departure);
	}


	/**
	 * converts the KLM json map to a departure object
	 * if data is missing for a field,
	 * the field will be null.
	 * 
	 * @param jsonMap
	 * @return Departure
	 */
	private Departure translateKLMMessage(Map<String, Object> jsonMap) {
		Departure departure = new Departure();
		departure.setAirline((String)jsonMap.get("airline"));
		departure.setFlightNo((String)jsonMap.get("flightNo"));
		departure.setDestination((String)jsonMap.get("destination"));
		departure.setOrigin((String)jsonMap.get("origin"));
		//TODO
		departure.setDate((String)jsonMap.get("date"));

		return departure;
	}
	/**
	 * converts the SAS json map to a departure object
	 * if data is missing for a field,
	 * the field will be null.
	 * 
	 * @param jsonMap
	 * @return Departure
	 */
	private Departure translateSASMessage(Map<String, Object> jsonMap) {
		Departure departure = new Departure();
		departure.setAirline((String)jsonMap.get("airline"));
		departure.setFlightNo((String)jsonMap.get("flightNo"));
		departure.setDestination((String)jsonMap.get("destination"));
		departure.setOrigin((String)jsonMap.get("origin"));
		departure.setArrivalDeparture((String)jsonMap.get("arrivalDeparture"));
		//TODO
		departure.setDate((String)jsonMap.get("dato") + ": "  +(String)jsonMap.get("tidspunkt"));

		return departure;
	}
	/**
	 * converts the SW json map to a departure object
	 * if data is missing for a field,
	 * the field will be null.
	 * 
	 * @param jsonMap
	 * @return Departure
	 */
	private Departure translateSWMessage(Map<String, Object> jsonMap) {
		Departure departure = new Departure();
		departure.setAirline((String)jsonMap.get("airline"));
		departure.setFlightNo((String)jsonMap.get("flightNo"));
		departure.setDestination((String)jsonMap.get("destination"));
		//TODO
		departure.setDate((String)jsonMap.get("date") + (String)jsonMap.get("departure"));

		return departure;
	}

	/**
	 * converts the json map to a departure object
	 * if data is missing for a field,
	 * the field will be null.
	 * @param jsonMap
	 * @return Departure
	 */
	public Departure translateMessage(Map<String, Object> jsonMap) {
		//TODO create this translation
		Departure departure = new Departure();
		if (isSASMessage(jsonMap)) {
			departure = translateSASMessage(jsonMap);
		} else if (isKLMMessage(jsonMap)) {
			departure = translateKLMMessage(jsonMap);
		} else if (isSWMessage(jsonMap)) {
			departure = translateSWMessage(jsonMap);
		}
		return departure;
	}

	private boolean isKLMMessage(Map<String, Object> jsonMap) {
		return (hasOriginAttribute(jsonMap));
	}

	private boolean hasArrivalDepatureAttribute(Map<String, Object> jsonMap) {
		return (jsonMap.get("arrivalDeparture") != null);
	}

	private boolean isSASMessage(Map<String, Object> jsonMap) {
		return hasArrivalDepatureAttribute(jsonMap);
	}

	private boolean hasOriginAttribute(Map<String, Object> jsonMap) {
		return (jsonMap.get("origin") != null);
	}

	private boolean isSWMessage(Map<String, Object> jsonMap) {
		return (!hasOriginAttribute(jsonMap));
	}
}
