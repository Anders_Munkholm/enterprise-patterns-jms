package com.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.model.Message;
import com.repository.SharedObjects;

import com.service.JmsMessageTemplateService;

import com.model.Company;

@Service
public class MessageRouterService {
	
	@Autowired
	private JmsMessageTemplateService jmsMessageTemplateService;

	
	public void initComapnies() {
		SharedObjects.initCompanies();
		
	}

	

	/**
	 * Sends a message the the reciever
	 * messageRouter donno might be a waste
	 * @param Message : the message that will be send
	 * @param String : reciever for whom the message is send to
	 */
	public void sendMessage(Message message, String recieverRoute) {
		jmsMessageTemplateService.getJmsTemplate().convertAndSend(recieverRoute, new Message("AirportInformationCenter","we are forwarding a message from  AirportInformationCenter the message is <" + message.getBody() ));
		
		
	}

	


}
