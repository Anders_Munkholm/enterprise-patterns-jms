package com.service;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import com.repository.SharedObjects;

@Service
public class ResequencerService {

	@Autowired
	private JmsMessageService jmsMessageService;
	@Autowired
	JmsMessageTemplateService jmsMessageTemplateService;

	public void saveResequencerMessage(Message message) throws JMSException {
		// TODO Auto-generated method stub
		String xmlMessage = jmsMessageService.extractTextMessageString(message);
		String resequencerId = message.getStringProperty("sequenceId");
		String messageTypeId = message.getStringProperty("messageTypeId"); //not used
		String messageSize = message.getStringProperty("messageSize"); 
		String messageNumber = message.getStringProperty("messageNumber");

		SharedObjects.addResequencerMessage(resequencerId,xmlMessage, messageNumber);
	}

	private int getMessageSequenceSize(Message message) throws JMSException {
		String messageSize = message.getStringProperty("messageSize");
		int messageSizeInt = Integer.parseInt(messageSize);
		return messageSizeInt;
	}

	public boolean isSequenceMessageComplete(Message message) throws JMSException {
		String resequencerId = message.getStringProperty("sequenceId");
		int messageSize = getMessageSequenceSize(message);
		if (SharedObjects.getResequencerSize(resequencerId) == messageSize ) {
			return true;
		}
		return false;
	}

	public void sendCompletedResequenceMessage(Map<String,String> resequencedMessages) throws JmsException, JMSException {
		System.out.println("start sending correct sequence to the aggregator");
		String uuid = UUID.randomUUID().toString();
		String messageSize = "" + resequencedMessages.size();
		//This should be send in a batch or something. It might be out of sequence again after sending .
		for (int messageNumber = 1; messageNumber <=  resequencedMessages.size(); messageNumber++) {
			String sequenceMessage = resequencedMessages.get(""+messageNumber);
			this.jmsMessageTemplateService.getJmsTemplate().send("Aggregator", 
					getResequenserMessageCreator(
							sequenceMessage, 
							messageNumber,
							uuid,
							messageSize
							));
		}

	}

	



	private MessageCreator getResequenserMessageCreator(String XmlMessage, int messageNumber,String uuid, String messageSize) {
		return new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				Message message = session.createTextMessage(XmlMessage);
				message.setStringProperty("messageNumber", ""+messageNumber);
				message.setStringProperty("messageSize", messageSize);
				message.setStringProperty("uuid",uuid);
				return message;
			}
		};
	}

	/**
	 * 
	 * @param message
	 * @return Map<messageNmber, XmlMessage>
	 * @throws JMSException
	 */
	public Map<String,String> getComletedSequenceMessages(Message message) throws JMSException {
		String resequencerId = message.getStringProperty("sequenceId");
		return SharedObjects.getFullResequencerMessageParts(resequencerId);
	}




}
