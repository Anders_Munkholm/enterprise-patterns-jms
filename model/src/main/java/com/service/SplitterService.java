package com.service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.service.JmsMessageService;
import com.service.JmsMessageTemplateService;

@Service
public class SplitterService {

	@Autowired
	private JmsMessageService jmsMessageService;

	@Autowired
	private JmsMessageTemplateService jmsMessageTemplateService;

	/**
	 * 
	 * @param message
	 * @return a Map which first element represents the messageType, the second is the messageText retrieved
	 * @throws XPathExpressionException
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws TransformerException 
	 */
	public Map<String,String> splitFlightInformation(String xmlText) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException, TransformerException {
		Map<String, String> xmlMap = new HashMap<String,String>(); 
		extractFlight(xmlMap, xmlText);
		extractPassenger(xmlMap, xmlText);
		extractLuggages(xmlMap,xmlText);
		return xmlMap;
	}

	private void extractFlight(Map<String, String> xmlMap, String xmlText) throws TransformerException, XPathExpressionException, SAXException, IOException, ParserConfigurationException {
		String expression = "/FlightDetailsInfoResponse/Flight";

		NodeList flightList = convertToNodeList(xmlText, expression);

		if (flightList.getLength() == 1) {
			int index = 0;
			Element flightElement = getNodeElement(flightList.item(index));
			String messageType = getflightListDestination(flightElement, index);
			String flightString = getXmldocumentFromElement(flightElement);
			xmlMap.put(messageType, flightString);
		} else {
			//TODO picnic 

		}

	}

	private String getflightListDestination(Element flightElement, int index) {
		return "Flight";
	}

	private void extractLuggages(Map<String, String> map, String xmlText) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException, TransformerException {
		String expression = "/FlightDetailsInfoResponse/Luggage";
		NodeList luggageList = convertToNodeList(xmlText, expression);

		for(int index = 0; index < luggageList.getLength(); index++) {
			Element luggageElement = getNodeElement(luggageList.item(index));
			String messageType = getLuggageDestination(luggageElement, index);
			String luggageString = getXmldocumentFromElement(luggageElement);
			map.put(messageType, luggageString);	
		}

	}


	private String getXmldocumentFromElement(Element element) throws TransformerException {
		TransformerFactory tFact = TransformerFactory.newInstance();
		Transformer trans = tFact.newTransformer();

		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		DOMSource source = new DOMSource(element);

		trans.transform(source, result);
		return writer.toString();
	}

	private void extractPassenger(Map<String, String> map, String xmlText) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException, TransformerException {
		String expression = "/FlightDetailsInfoResponse/Passenger";

		NodeList passengerList = convertToNodeList(xmlText, expression);

		if (passengerList.getLength() == 1) {
			int index = 0;
			Element passengerElement = getNodeElement(passengerList.item(index));
			String messageType = getPassengerDestination(passengerElement, index);
			String  passengerString = getXmldocumentFromElement(passengerElement);
			map.put(messageType,  passengerString);
		} else {
			//TODO picnic 

		}
	}





	private String getPassengerDestination(Element passengerElement, int index) {
		return "Passenger";
	}

	private String getLuggageDestination(Element luggageElement, int index) {
		NodeList categoryList = luggageElement.getElementsByTagName("Category");
		if (categoryList.getLength() == 1) {
			return categoryList.item(0).getTextContent() + (index + 1);
		} else {
			return "";
		}
	}






	private Element getNodeElement(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			return (Element) node;
		}
		System.out.println("error");
		return null;
	}

	private NodeList convertToNodeList(String xmlText,String expression) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(
				new InputSource(
						new StringReader(xmlText)));
		return (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
	}



	public String extractMessageText(Message message) {
		return this.jmsMessageService.extractTextMessageString(message);
	}

	/**
	 * 
	 * @param splitMessages the first element is the messageType identifyinng what to do with the element. The second element is the xmlMessageText to be send
	 */
	public void sendSplitMessages(Map<String, String> splitMessages) {
		System.out.println("sending splitted messages");
		String uuidResequencer =  UUID.randomUUID().toString();
		sendFlightAndPassenger(uuidResequencer, splitMessages, uuidResequencer);
		int messageNumber = 3; //this is the number that the next messsage will have, ei send 3 messages prior to the loop, then the number has to be 4
		for (String messageType : splitMessages.keySet()) {
			if (messageType.contains("Normal")) {
				sendLuggage("Resequencer", 
						splitMessages, 
						messageType,
						uuidResequencer, 
						splitMessages.size(), 
						messageNumber);
				messageNumber++;
			} else if (messageType.contains("Large")) {
				sendLuggage("Resequencer", 
						splitMessages, 
						messageType,
						uuidResequencer, 
						splitMessages.size(), 
						messageNumber);
				messageNumber++;
			} 
		}
	}




	private void sendLuggage(String destination, Map<String, String> splitMessages, String messageType,
			String uuidResequencer, int size, int messageNumber) {
		sendSplitMessage(destination, 
				splitMessages.get(messageType), 
				uuidResequencer, 
				""+ splitMessages.size(), 
				messageNumber);
		
	}

	

	

	private void sendFlightAndPassenger(String destination, Map<String, String> splitMessages, String uuidResequencer) {
		sendSplitMessage("Resequencer", 
				splitMessages.get("Flight"), 
				uuidResequencer,
				"" +splitMessages.size(), 
				1);
		sendSplitMessage("Resequencer", 
				splitMessages.get("Passenger"), 
				uuidResequencer, 
				"" +splitMessages.size(), 
				2);
		
	}


	public void sendSplitMessage(String destination, String xmlString,String uuidResequencer, String messageSize, int messageNumber) {
		if (jmsMessageTemplateService == null ) {
			jmsMessageTemplateService = new JmsMessageTemplateService();
		}
		jmsMessageTemplateService.getJmsTemplate()
		.send(
				destination, 
				createSplitMessage(xmlString, uuidResequencer, messageSize,messageNumber)
				);
	}


	private MessageCreator createSplitMessage(String xmlString,String uuidResequencer,String messageSize,int messageNumber) {
		return new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				Message message = session.createTextMessage(xmlString);
				message.setStringProperty("sequenceId", uuidResequencer);
				message.setStringProperty("messageSize", messageSize);
				message.setStringProperty("messageNumber", "" + messageNumber);
				return message;
			}
		};
	}

	public String createXMLDocument() throws ParserConfigurationException, TransformerException {
		Document xmlDocument = createFlightDetailsInfoResponseXmlDocumentString();

		TransformerFactory tFact = TransformerFactory.newInstance();
		Transformer trans = tFact.newTransformer();

		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		DOMSource source = new DOMSource(xmlDocument);
		trans.transform(source, result);
		return writer.toString();
	}

	private Document createFlightDetailsInfoResponseXmlDocumentString() throws ParserConfigurationException {
		DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
		DocumentBuilder build = dFact.newDocumentBuilder();
		Document doc = build.newDocument();
		Element root = doc.createElement("FlightDetailsInfoResponse");
		Element flightElement = createFlightElement(doc);
		Element passengerElement = createPassengerElement(doc);
		Element luggageNormalElement = createLuggageElement(doc, "CA937200305252", "1", "Normal", "17.3");
		Element luggageHeavyElement = createLuggageElement(doc, "CA937200305252", "2" ,"Large", "22.7");
		doc.appendChild(root);
		root.appendChild(flightElement);
		root.appendChild(passengerElement);
		root.appendChild(luggageNormalElement);
		root.appendChild(luggageHeavyElement);

		return doc;

	}

	private Element createLuggageElement(Document doc,String idText, String identificationText, String categoryText,String weightText) {
		Element luggage = doc.createElement("Luggage");
		Element id = doc.createElement("Id");
		Element identification = doc.createElement("Identification");
		Element category = doc.createElement("Category");
		Element weight = doc.createElement("Weight");

		Text idTextNode = doc.createTextNode(idText);
		Text identificationTextNode = doc.createTextNode(identificationText);
		Text categoryTextNode = doc.createTextNode(categoryText);
		Text weightTextNode = doc.createTextNode(weightText);

		luggage.appendChild(id);
		luggage.appendChild(identification);
		luggage.appendChild(category);
		luggage.appendChild(weight);
		id.appendChild(idTextNode);
		identification.appendChild(identificationTextNode);
		category.appendChild(categoryTextNode);
		weight.appendChild(weightTextNode);
		return luggage;
	}

	private Element createPassengerElement(Document doc) {
		Element passenger = doc.createElement("Passenger");
		Element reservationNumber = doc.createElement("ReservationNumber");
		Element firstName = doc.createElement("FirstName");
		Element lastName = doc.createElement("LastElement");

		Text reservationNumberTextNode = doc.createTextNode("CA937200305251");
		Text firstNameTextNode = doc.createTextNode("Anders");
		Text lastNameTextNode = doc.createTextNode("Munkholm");

		passenger.appendChild(reservationNumber);
		passenger.appendChild(firstName);
		passenger.appendChild(lastName);
		reservationNumber.appendChild(reservationNumberTextNode);
		firstName.appendChild(firstNameTextNode);
		lastName.appendChild(lastNameTextNode);

		return passenger;
	}

	private Element createFlightElement(Document doc) {
		Element flight = doc.createElement("Flight");
		Element origin = doc.createElement("Origin");
		Element destination = doc.createElement("Destination");
		Text originTextNode = doc.createTextNode("ARLANDA");
		Text destinationTextNode = doc.createTextNode("LONDON");


		flight.appendChild(origin);
		flight.appendChild(destination);
		origin.appendChild(originTextNode);
		destination.appendChild(destinationTextNode);
		return flight;
	}






}


