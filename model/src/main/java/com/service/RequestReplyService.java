package com.service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.stereotype.Service;

import com.model.Airplane;

import com.service.JmsMessageTemplateService;


@Service
public class RequestReplyService {

	@Autowired
	private JmsMessageTemplateService jmsMessageTemplateService;
	
	
	
	
	
	
	/**
	 * creates an overloaded MessagePostProcessor and sets the jmsMessageId as the correlationId
	 * It will also create a new messageId for the new mesage that will be send
	 * @param messageModel 
	 * @return MessagePostProcessor with Correlation ID
	 * 
	 */
	private MessagePostProcessor getReplyMessagePostProcessor(Message jmsMessage) {
		return new MessagePostProcessor() {
			@Override
			public Message postProcessMessage(Message message) throws JMSException {
				//the message id cannot be set with setJMSID  it has to be done manually inside the body that is send(maybe a json object). 

	            message.setJMSCorrelationID(jmsMessage.getJMSMessageID());
	            
	            System.out.println("the ID recieved and is set to the correlation ID" + jmsMessage.getJMSMessageID());
	            
	            return message;
	        }
	    };
	}

	


	public void sendReplyMessage(Destination destination,Message jmsMessage, Airplane airplane) {
		jmsMessageTemplateService
		.getJmsTemplate()
		.convertAndSend(
				destination, 
				getGetFlightETA(airplane),
				getReplyMessagePostProcessor(jmsMessage)
				);
		
	}
	
	
	/**
	 * 
	 * @param airplane
	 * @return new message with an eta on when they arrive / can arrive
	 */
	public com.model.Message getGetFlightETA(Airplane airplane) {
		
			return new com.model.Message("Bluff City Airport","ETA 900 hours from now, good luck " + airplane.getFlightNumber());
		
	}
	public Message send(String destination, String bodyContent) {
		if (jmsMessageTemplateService == null ) {
			jmsMessageTemplateService = new JmsMessageTemplateService();
		}
		Message m = jmsMessageTemplateService.getJmsTemplate().sendAndReceive(destination,new MessageCreator() {
	    
		      @Override
		      public Message createMessage(Session session) throws JMSException {
		        Message message = session.createTextMessage(bodyContent);
		        
		        return message;
		      }
		    });
		
		return m;
		
	}

}
