package com.service;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.model.Airplane;

import com.service.JmsMessageTemplateService;

@Service
public class JmsMessageService {


	@Autowired
	JmsMessageTemplateService jmsMessageTemplateService;

	public String extractTextMessageString(Message message) {
		try {
			String messageText = null;
			if (message instanceof TextMessage) {
				//gets a text from message
				messageText = getText(message);
				return messageText;
			}
		} catch (JMSException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getText(Message message) throws JMSException {
		TextMessage textMessage = (TextMessage) message;
		return textMessage.getText();
	}

	public void sendTextMessage(String destination, String xmlMessage) {
		if (jmsMessageTemplateService == null) {
			jmsMessageTemplateService = new JmsMessageTemplateService();
		}
		this.jmsMessageTemplateService.getJmsTemplate().send(destination, new MessageCreator() {

			@Override
			public Message createMessage(Session session) throws JMSException {
				Message message = session.createTextMessage(xmlMessage);
				return message;
			}
			
		});

	}

	




}
