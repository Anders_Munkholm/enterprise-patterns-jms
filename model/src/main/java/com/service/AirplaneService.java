package com.service;

import java.io.IOException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.model.Airplane;

@Service
public class AirplaneService {

	public Airplane convertMessageTextToAirplane(Message message) {
		try {
			String messageText = null;
			if (message instanceof TextMessage) {
				//gets a text from message
                messageText = getText(message);
                //converts the text to object
                ObjectMapper mapper = new ObjectMapper();
                return  mapper.readValue(messageText, Airplane.class);
            }
		} catch (JMSException | IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getText(Message message) throws JMSException {
		TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
	}

}
