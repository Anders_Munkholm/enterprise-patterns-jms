package com.service;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.repository.SharedObjects;
@Service
public class JmsMessageTemplateService {

	/**
	 * 
	 * @param configurableApplicationContext
	 * @return the configurableApplicationContext that was saved
	 */
	public ConfigurableApplicationContext saveConfigurableApplicationContext(ConfigurableApplicationContext configurableApplicationContext) {
		SharedObjects.setConfigurableApplicationContext(configurableApplicationContext);
		return configurableApplicationContext;
	}
	
	public void configurableApplicationContext(ConfigurableApplicationContext configurableApplicationContext) {
		saveConfigurableApplicationContext(configurableApplicationContext);
	}
	
	/**
	 * The jms template contains methods for sending messages through jms
	 * @return a jmsTemplate used for sending messages
	 */
	public JmsTemplate getJmsTemplate() {
		return SharedObjects.getConfigurableApplicationContext().getBean(JmsTemplate.class);
	}
}
