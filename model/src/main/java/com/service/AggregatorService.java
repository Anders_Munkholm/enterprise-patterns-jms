package com.service;

import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.repository.SharedObjects;

@Service
public class AggregatorService {
	@Autowired
	private JmsMessageService jmsMessageService;
	@Autowired
	private JmsMessageTemplateService jmsMessageTemplateService;
	@Autowired
	private ResequencerService resequencerService;
	
	
	public void saveAggregatorMessage(Message message) throws JMSException {
				String xmlMessage = jmsMessageService.extractTextMessageString(message);
				String aggregatorId = message.getStringProperty("uuid");
				String messageTypeId = message.getStringProperty("messageTypeId"); //not used
				String messageSize = message.getStringProperty("messageSize"); 
				String messageNumber = message.getStringProperty("messageNumber");

				SharedObjects.addAggregatorMessage(aggregatorId,xmlMessage, messageNumber);
	}

	public boolean isAggregatorMessageComplete(Message message) throws JMSException {
		String aggregatorId = message.getStringProperty("uuid");
		int messageSize = getMessageSequenceSize(message);
		if (SharedObjects.getAggregatorSize(aggregatorId) == messageSize ) {
			return true;
		}
		return false;
	}

	

	private int getMessageSequenceSize(Message message) throws JMSException {
		String messageSize = message.getStringProperty("messageSize");
		int messageSizeInt = Integer.parseInt(messageSize);
		return messageSizeInt;
	}

	
	/**
	 * 
	 * @param message
	 * @return Map<messageNmber,xmlMessage>
	 * @throws JMSException
	 */
	public Map<String, String> getComletedSequenceMessages(Message message) throws JMSException {
		String resequencerId = message.getStringProperty("uuid");
		return SharedObjects.getFullAggregatorMessageParts(resequencerId);
	}

	/**
	 * 
	 * @param sequencedMessages<messageNmber,xmlMessage>
	 * @return merged xml doc
	 */
	public String mergeMessages(Map<String, String> sequencedMessages) {
		String finalXmlMessage = "";
		for (String messageNumber : sequencedMessages.keySet()) {
			finalXmlMessage += sequencedMessages.get(messageNumber);
		}
		return finalXmlMessage;
		
	}


	public void sendCompletedAggregatorMessage(String xmlComplete) {
		this.jmsMessageService.sendTextMessage("ResequencerCompleteMessage", xmlComplete);
	}

}
