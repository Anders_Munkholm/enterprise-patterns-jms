package com.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.jms.Message;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Repository;

import com.model.Company;

@Repository
public class SharedObjects {
	
	private static ConfigurableApplicationContext configurableApplicationContext;
	private static ArrayList<Company> companies = new ArrayList<Company>();
	private static Map<String, Map<String,String>> resequencerMessageParts = new HashMap<String,Map<String,String>>();
	private static Map<String,Map<String,String>> aggregatorMessageParts = new HashMap<String,Map<String,String>>();
	
	public static void setConfigurableApplicationContext(ConfigurableApplicationContext ConfigurableApplicationContextParameter) {
		configurableApplicationContext = ConfigurableApplicationContextParameter;
	}
	
	public static ConfigurableApplicationContext getConfigurableApplicationContext() {
		return configurableApplicationContext;
	}
	
	public static ArrayList<Company> getCompanies() {
		return companies;
	}
	
	public static Company addCompanies(Company company) {
		companies.add(company);
		return company;
	}
	
	public static Map<String,String> getResequencerMessageParts(String key) {
		return resequencerMessageParts.get(key);
		
	}
	
	public static void initCompanies() {
		
			addCompanies(new Company("SAS", "ScandinavianAirlineService"));
			addCompanies(new Company("SWA", "SouthWestAirlines"));
		
	}

	public static void addResequencerMessage(String resequencerId,String message, String messageNumber) {
		if (resequencerMessageParts.get(resequencerId) == null) {
			Map<String,String> messageMap = new HashMap<String, String>();
			resequencerMessageParts.put(resequencerId, messageMap);
		} 
			resequencerMessageParts.get(resequencerId).put(messageNumber, message);
		
	}

	public static int getResequencerSize(String resequencerId) {
		if (resequencerMessageParts.get(resequencerId) == null) {
			return 0;
		}
		return resequencerMessageParts.get(resequencerId).size();
	}

	public static  Map<String,String> getFullResequencerMessageParts(String resequencerId) {
		
		return resequencerMessageParts.get(resequencerId);
		
	}

	public static void addAggregatorMessage(String aggregatorId, String xmlMessage, String messageNumber) {
		if (aggregatorMessageParts.get(aggregatorId) == null) {
			Map<String,String> messageMap = new HashMap<String, String>();
			aggregatorMessageParts.put(aggregatorId, messageMap);
		} 
		aggregatorMessageParts.get(aggregatorId).put(messageNumber, xmlMessage);
		
	}

	public static int getAggregatorSize(String aggregatorId) {
		if (aggregatorMessageParts.get(aggregatorId) == null) {
			return 0;
		}
		return aggregatorMessageParts.get(aggregatorId).size();
	}

	public static Map<String, String> getFullAggregatorMessageParts(String resequencerId) {

		return aggregatorMessageParts.get(resequencerId);
	}
	
	

}
