package com.patterns.listener;

import java.io.IOException;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.SessionAwareMessageListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Message;
import com.service.AirplaneService;
import com.service.JmsMessageTemplateService;
import com.service.RequestReplyService;
import com.model.Airplane;

@Component
public class RequestReplyListener {

	@Autowired
	private RequestReplyService requestReplyService;
	@Autowired
	private AirplaneService airplaneService;
	

	@JmsListener(destination = "AirportInformationCenterRequest", containerFactory = "myFactory")
	public void requestMessage(javax.jms.Message message, javax.jms.Session session) {
		try {
			Destination destination = message.getJMSReplyTo();
			Airplane airplane = airplaneService.convertMessageTextToAirplane(message);
			this.requestReplyService.sendReplyMessage(destination, message, airplane);
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
	

	


	

	
	





}
