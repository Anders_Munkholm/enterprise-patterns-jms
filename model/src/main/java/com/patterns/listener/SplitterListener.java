package com.patterns.listener;

import java.util.ArrayList;
import java.util.Map;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.model.Airplane;
import com.service.SplitterService;

@Component
public class SplitterListener {
	
	
	@Autowired
	SplitterService splitterService;
	
	@JmsListener(destination = "Splitter", containerFactory = "myFactory")
	public void splitMessage(javax.jms.Message message) {
		try {
			String xmlText = splitterService.extractMessageText(message);
			//This map has been tested
			Map<String,String> splitMessages = splitterService.splitFlightInformation(xmlText);
			splitterService.sendSplitMessages(splitMessages);
			
		} catch (Exception e) {
			System.out.println("Error");
			e.printStackTrace();
		}
	}
	
	
	
	
	
}
