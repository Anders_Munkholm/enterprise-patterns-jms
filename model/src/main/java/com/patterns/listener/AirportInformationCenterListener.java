package com.patterns.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.model.Message;
import com.service.MessageRouterService;

@Component
public class AirportInformationCenterListener {
	
	
	/*
	 * this path recieves messages for the airport information center
	 * 
	 */
    @JmsListener(destination = "AirportInformationCenter", containerFactory = "myFactory")
    public void receiveMessage(Message message) {
    	
        System.out.println("AirportInformationCenter Received <" + message + ">");
        
    }
    
    /*
     * this path is for recieving error messages
     */
    @JmsListener(destination = "AirportInformationCenterError", containerFactory = "myFactory")
    public void receiveErrorMessage(Message message) {
    	
        System.out.println("AirportInformationCenter Received <" + message + ">");
        
    }
    
   
    
    
    
    
    
    
    
	
	
}
