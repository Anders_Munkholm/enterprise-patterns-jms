package com.patterns.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.model.Message;
import com.service.MessageRouterService;

@Component
public class MessageRouterListener {
	
	
	@Autowired
	private MessageRouterService messageRouterService;
    
    /*
     * This part of the message router recieves and forwards the messages to the routes that the information center asks it do send it to
     */
    @JmsListener(destination = "AirportInformationCenterMessageRouterSender", containerFactory = "myFactory")
    public void sendMessage(Message message) {
    	if (isScandinavianAirlineServiceMessage(message)) {
    		this.messageRouterService.sendMessage(message, "ScandinavianAirlineService");
    	} else if (isSouthWestAirlinesMessage(message)) {
    		this.messageRouterService.sendMessage(message, "SouthWestAirlines");
    	} else {
    		this.messageRouterService.sendMessage(message, "AirportInformationCenterError");
    	}
    }
    /**
     * 
     * @param message
     * @return true if the message header tells the router to send a message to sas
     */
    public boolean isScandinavianAirlineServiceMessage(Message message) {
    	return message.getHead().equals("AirportInformationCenter SAS");
    }
    /**
     * 
     * @param message
     * @return true if the message header tells the router to send a message to swa
     */
    public boolean isSouthWestAirlinesMessage(Message message) {
    	return message.getHead().equals("AirportInformationCenter SWA");
    }
    
    
    
    /*
	 * outcomment this line of code if you want the router to be able to recieve messages and send to somewhere else than the main listener
	 */
//    @JmsListener(destination = "AirportInformationCenterMessageRouterReciever", containerFactory = "myFactory")
//    public void receiveMessage(Message message) {
//    	this.messageRouterService.sendMessage(message, message.getHead());
//    }
	
}
