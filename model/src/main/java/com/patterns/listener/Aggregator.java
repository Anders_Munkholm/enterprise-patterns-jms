package com.patterns.listener;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.service.AggregatorService;
import com.service.ResequencerService;
@Component
public class Aggregator {
	
	@Autowired
	private AggregatorService aggregatorService;
	


	@JmsListener(destination = "Aggregator", containerFactory = "myFactory")
	public void resequencer(javax.jms.Message message) {
		try {
			aggregatorService.saveAggregatorMessage(message);
			if (aggregatorService.isAggregatorMessageComplete(message)) {
				System.out.println("aggregator message is complete");
				Map<String,String> sequencedMessages = aggregatorService.getComletedSequenceMessages(message);
				String finalXmlMessage = aggregatorService.mergeMessages(sequencedMessages);
				aggregatorService.sendCompletedAggregatorMessage(finalXmlMessage);
			} 	
		} catch(Exception e) {
			//do stuff TODO
			e.printStackTrace();
		}
	}

}
