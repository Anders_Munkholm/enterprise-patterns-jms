package com.patterns.listener;



import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.model.Departure;
import com.service.TranslatorService;



@Component
public class TranslatorListener {

	@Autowired
	private TranslatorService translatorService;

	@JmsListener(destination = "translator", containerFactory = "myFactory")
	public void translatorListener(javax.jms.Message message) {
		try {
			Map<String,Object> jsonMap = translatorService.getMessageJsonMap(message);
			Departure departureMessage = translatorService.translateMessage(jsonMap);
			translatorService.sendTranslatedMessage(departureMessage);
		} catch(Exception e) {
			e.printStackTrace();
			//TODO error handling
		}

	}
	
	
	


}
