package com.patterns;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.NamingException;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.service.JmsMessageTemplateService;


// from http://www.source4code.info/2014/11/jms-publish-subscribe-messaging-example-activemq-maven.html
public class PublisherTemplate {



    private Connection connection;
    private Session session;
    private MessageProducer updateProducer;
    private JmsMessageTemplateService jmsMessageTemplateService;
    
    
    public PublisherTemplate() {
    	jmsMessageTemplateService = new JmsMessageTemplateService();
    }

    public void create(String clientId, String destination) throws JMSException {

        createConnection("tcp://localhost:61616",clientId);
        setupPublisherChannel(destination);
        startConnection();
    }
    
    public void startConnection() throws JMSException {
    	connection.start();
    }
    
    private void setupPublisherChannel(String destination) throws JMSException {
    	session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination destinationTopic = session.createTopic(destination);
        updateProducer = session.createProducer(destinationTopic);
   }
    


    public void closeConnection() throws JMSException {
        connection.close();
    }
    
    public void release() throws JMSException {
		if (connection != null) {
			connection.stop();
			connection.close();
		}
	}
    
    private void createConnection(String brookerUrl, String clientId) throws JMSException {
    	connection = this.jmsMessageTemplateService
    			.getJmsTemplate()
    			.getConnectionFactory()
    			.createConnection();
    	connection.setClientID(clientId);
    }

    public void sendMessage(String firstName, String lastName) throws JMSException {
        String text = firstName + " " + lastName;

        // create a JMS TextMessage
        TextMessage textMessage = session.createTextMessage(text);

        // send the message to the topic destination
        updateProducer.send(textMessage);

       
    }
    
    
    
    
}

