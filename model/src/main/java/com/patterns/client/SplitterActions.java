package com.patterns.client;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.service.JmsMessageService;
import com.service.JmsMessageTemplateService;
import com.service.SplitterService;

public class SplitterActions {
	
	private JmsMessageService jmsMessageService;
	private SplitterService splitterService;
	
	public SplitterActions() {
		splitterService = new SplitterService();
		jmsMessageService = new JmsMessageService();
	}
	
	public void doSplitterActions() {
		try {
			String xmlText = splitterService.createXMLDocument();
			this.jmsMessageService.sendTextMessage("Splitter", xmlText);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
