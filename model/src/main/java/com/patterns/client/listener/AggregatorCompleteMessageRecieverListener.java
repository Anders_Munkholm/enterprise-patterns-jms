package com.patterns.client.listener;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.service.JmsMessageService;

@Component
public class AggregatorCompleteMessageRecieverListener {

	@Autowired
	JmsMessageService jmsMessageService;
	
	@JmsListener(destination = "ResequencerCompleteMessage", containerFactory = "myFactory")
	public void resequencer(javax.jms.Message message) {
		try {
			System.out.println("ResequencerCompleteMessage");
			System.out.println(jmsMessageService.extractTextMessageString(message));
			
		} catch(Exception e) {
			//do stuff TODO
			e.printStackTrace();
		}
	}
}
