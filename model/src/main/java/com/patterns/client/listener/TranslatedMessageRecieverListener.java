package com.patterns.client.listener;

import java.util.Map;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import com.model.Departure;

@Component
public class TranslatedMessageRecieverListener {


	@JmsListener(destination = "TranslatedMessageReciever", containerFactory = "myFactory")
	public void resequencer(Departure departure) {
		System.out.println("departure attributes"
				+ "\nairline : " + departure.getAirline()
				+ "\narrivalDeparture : " + departure.getArrivalDeparture()
				+ "\ndate : " + departure.getDate()
				+ "\ndestination : " + departure.getDestination()
				+ "\nflightNo : " + departure.getFlightNo()
				+ "\norigin : " + departure.getOrigin());
	}
}
