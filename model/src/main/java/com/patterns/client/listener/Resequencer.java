package com.patterns.client.listener;

import java.util.ArrayList;
import java.util.Map;

import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.service.JmsMessageService;
import com.service.ResequencerService;
@Component
public class Resequencer {

	@Autowired
	private ResequencerService resequencerService;


	@JmsListener(destination = "Resequencer", containerFactory = "myFactory")
	public void resequencer(javax.jms.Message message) {
		try {
			resequencerService.saveResequencerMessage(message);
			if (resequencerService.isSequenceMessageComplete(message)) {
				System.out.println("message is complete" + resequencerService.getComletedSequenceMessages(message).get("5"));
				Map<String,String> resequencedMessages = resequencerService.getComletedSequenceMessages(message);
				resequencerService.sendCompletedResequenceMessage(resequencedMessages);
			} 	
		} catch(Exception e) {
			//do stuff TODO
			e.printStackTrace();
		}
	}
}
