package com.patterns.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.service.JmsMessageTemplateService;
import com.service.RequestReplyService;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQQueue;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
 

public class RequestReplyActions {
	
	@Autowired
	RequestReplyService requestReplyService = new RequestReplyService();
	
	public RequestReplyActions() {
		
	}
	
	
	public void doRequestReply() {
		
		Message message = requestReplyService.send("AirportInformationCenterRequest", createBodyJSONContentWithFlightNumber(100));
		try {
			String messageText = null;
			if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                messageText = textMessage.getText();
                System.out.println("messageText = " + messageText);
                System.out.println("the jms messageID from the airportcenter is " + textMessage.getJMSMessageID());
                System.out.println("the messageId that we send to the applicaiton which is the correlationId is " + message.getJMSCorrelationID());
              
            }
			
		
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String createBodyJSONContentWithFlightNumber(int flightNumber) {
		JSONObject bodyContent = new JSONObject();

	      try {
	    	  bodyContent.put("flightNumber", flightNumber);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		return bodyContent.toString();
	}

}
