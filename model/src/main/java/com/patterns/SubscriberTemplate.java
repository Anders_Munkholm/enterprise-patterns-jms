package com.patterns;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.service.JmsMessageTemplateService;

public class SubscriberTemplate implements MessageListener {

 

    private static final String NO_GREETING = "no greeting";

    private String clientId;
    private Connection connection;
    private Session session;
    private MessageConsumer updateConsumer ;
    private JmsMessageTemplateService jmsMessageTemplateService;
    
    public SubscriberTemplate() {
    	jmsMessageTemplateService = new JmsMessageTemplateService();
    }

    public void createSubsriber(String clientId, String destination) throws JMSException {
        createConnection("tcp://localhost:61616", clientId);
        setupSubsriberChannel(destination);
        startConnection();
    }
    
    @Override
	public void onMessage(Message message) {
		try {
			TextMessage textMsg = (TextMessage) message; // assume cast always works
			String newState = textMsg.getText();
			System.out.println("sas has recieved"+newState);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

    private void createConnection(String brookerUrl, String clientId) throws JMSException {
    	connection = this.jmsMessageTemplateService.getJmsTemplate().getConnectionFactory().createConnection();
    	connection.setClientID(clientId);
    }
    
    private void setupSubsriberChannel(String destination) throws JMSException {
    	 session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

         Destination topic = session.createTopic(destination);

         updateConsumer  = session.createConsumer(topic);
         updateConsumer.setMessageListener(this);
    }
    
    public void closeConnection() throws JMSException {
        connection.close();
    }
    
    public void startConnection() throws JMSException {
    	connection.start();
    }
    
 


	
	
	
}
