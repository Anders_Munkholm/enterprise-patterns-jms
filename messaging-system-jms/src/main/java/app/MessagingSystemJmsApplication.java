package app;

import java.sql.Date;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.model.AirlineCompanyKLM;
import com.model.AirlineCompanySAS;
import com.model.AirlineCompanySW;
import com.model.Message;
import com.patterns.Publisher;
import com.patterns.PublisherTemplate;
import com.service.JmsMessageTemplateService;
import com.service.MessageRouterService;


@SpringBootApplication
@EnableJms
@ComponentScan({"com.patterns.listener","com.model","com.service"})
public class MessagingSystemJmsApplication {

	
	private static MessageRouterService messageRouterService = new MessageRouterService();
	private static JmsMessageTemplateService jmsMessageTemplateService = new JmsMessageTemplateService();

	
	
	
	public void registerBeans(ConfigurableApplicationContext context ){
	    BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(JmsTemplate.class);
	    CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();

	    builder.addPropertyValue("connectionFactory", cachingConnectionFactory);      // set property value
	    DefaultListableBeanFactory factory = (DefaultListableBeanFactory) context.getAutowireCapableBeanFactory();
	    factory.registerBeanDefinition("jmsTemplateName", builder.getBeanDefinition());
	}
	
	
	@Bean
	public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
			DefaultJmsListenerContainerFactoryConfigurer configurer) {

		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();


		// This provides all boot's default to this factory, including the message converter
		configurer.configure(factory, connectionFactory);

		// You could still override some of Boot's default if necessary.
		return factory;
	}




	@Bean // Serialize message content to json using TextMessage
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		return converter;
	}

	//configurating AMQ to run with two applications
	@Bean(initMethod = "start", destroyMethod = "stop")
	public BrokerService broker() throws Exception {
		final BrokerService broker = new BrokerService();
		broker.addConnector("tcp://localhost:61616");
		broker.addConnector("vm://localhost");
		broker.setPersistent(false);
		return broker;
	}

	public static void main(String[] args) {
		// Launch the application
		ConfigurableApplicationContext context = SpringApplication.run(MessagingSystemJmsApplication.class, args);
     
		JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
	
		jmsMessageTemplateService.saveConfigurableApplicationContext(context);
		messageRouterService.initComapnies();
		// Send a message with a POJO - the template reuse the message converter
		System.out.println("Sending a message");
		
		jmsTemplate.convertAndSend("AirportInformationCenterMessageRouterSender", new Message("AirportInformationCenter SAS", "message for the body field"));
		jmsTemplate.convertAndSend("AirportInformationCenterMessageRouterSender", new Message("AirportInformationCenter SWA", "message for the body field"));
		jmsTemplate.convertAndSend("AirportInformationCenterPublish", new Message("AirportInformationCenter SWA", "message for the body field"));
	//	PublisherTemplate publish = new PublisherTemplate();
//		try {
//			publish.create("publishAir","AirportInformationCenterPublish");
//			for (int i = 0; 0< 1000; i++) {
//				publish.sendMessage("hello", "something" + i);
//				System.out.println("sending a message to the subscribers");
//				Thread.sleep(30000);
//			}
//		} catch (JMSException | InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		AirlineCompanyKLM airlineCompanyKLM = new AirlineCompanyKLM("KLM", "154", "San Diego", "Schipol", "March 06 2017 16:45");
		AirlineCompanySAS airlineCompanySAS = new AirlineCompanySAS("SAS", "SK 239", "JFK", "CPH", "D", "6. marts 2017", "16:45");
		AirlineCompanySW airlineCompanySW = new AirlineCompanySW("South West Airline", "SW056", "New York", "03/06/2017", "09:45 PM");
		jmsTemplate.convertAndSend("translator", airlineCompanyKLM);
		jmsTemplate.convertAndSend("translator", airlineCompanySAS);
		jmsTemplate.convertAndSend("translator", airlineCompanySW);
		
	}




}
