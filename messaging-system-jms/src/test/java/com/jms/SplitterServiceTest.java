package com.jms;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;
import static org.mockito.Matchers.notNull;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactory;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.service.SplitterService;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;

public class SplitterServiceTest {
	
	private SplitterService splitterService;
	private Document xmlDocument;
	
	@Before
	public void setup() throws ParserConfigurationException {
		splitterService = new SplitterService();
		xmlDocument = createFlightDetailsInfoResponseXmlDocumentString();
	}
	
	@Test
	public void testcreateFlightDetailsInfoResponseXmlDocumentString() throws TransformerException {
		TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer trans = tFact.newTransformer();

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(xmlDocument);
        trans.transform(source, result);
        assertThat(writer.toString(), not(nullValue()));
	}

	@Test
	public void extractMessageTextTest() throws ParserConfigurationException, TransformerException, XPathExpressionException, SAXException, IOException {
		//creates a document object
		DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
        DocumentBuilder build = dFact.newDocumentBuilder();
        Document doc = build.newDocument();
		
		//gets elements needed for the test
		String xmlText = getElementXmlText(xmlDocument);
		Map<String, String> map = this.splitterService.splitFlightInformation(xmlText);
		Element luggageNormalElement = createLuggageElement(doc, "CA937200305252", "1", "Normal", "17.3");
		Element luggageHeavyElement = createLuggageElement(doc, "CA937200305252", "2" ,"Large", "22.7");
		Element passengerElement = this.createPassengerElement(doc);
		Element flightElement = this.createFlightElement(doc);
		String luggageNormalText = getElementXmlText(luggageNormalElement);
		String luggageLargeText = getElementXmlText(luggageHeavyElement);
		String passengerText = getElementXmlText(passengerElement);
		String flightText = getElementXmlText(flightElement);
		
		//tests the method
		assertThat(xmlText, not(nullValue()));
		assertThat(map, not(nullValue()));
		assertThat(map.get("Normal1"), is(luggageNormalText));
		assertThat(map.get("Large2"), is(luggageLargeText));
		assertThat(map.get("Passenger"), is(passengerText));
		assertThat(map.get("Flight"), is(flightText));
		//assertThat(map.get("Normal"), is(luggageNormalText));
		
        
	}
	
	private String getElementXmlText(Document document) throws TransformerException {
		TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer trans = tFact.newTransformer();

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(document);

        trans.transform(source, result);
		return writer.toString();
	}
	
	private String getElementXmlText(Element element) throws TransformerException {
		TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer trans = tFact.newTransformer();

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(element);

        trans.transform(source, result);
		return writer.toString();
	}
	
	private Document createFlightDetailsInfoResponseXmlDocumentString() throws ParserConfigurationException {
		DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
        DocumentBuilder build = dFact.newDocumentBuilder();
        Document doc = build.newDocument();
        Element root = doc.createElement("FlightDetailsInfoResponse");
        Element flightElement = createFlightElement(doc);
        Element passengerElement = createPassengerElement(doc);
        Element luggageNormalElement = createLuggageElement(doc, "CA937200305252", "1", "Normal", "17.3");
        Element luggageHeavyElement = createLuggageElement(doc, "CA937200305252", "2" ,"Large", "22.7");
        doc.appendChild(root);
        root.appendChild(flightElement);
        root.appendChild(passengerElement);
        root.appendChild(luggageNormalElement);
        root.appendChild(luggageHeavyElement);
        
		return doc;
		
	}

	private Element createLuggageElement(Document doc,String idText, String identificationText, String categoryText,String weightText) {
		Element luggage = doc.createElement("Luggage");
		Element id = doc.createElement("Id");
		Element identification = doc.createElement("Identification");
		Element category = doc.createElement("Category");
		Element weight = doc.createElement("Weight");
		
		Text idTextNode = doc.createTextNode(idText);
		Text identificationTextNode = doc.createTextNode(identificationText);
		Text categoryTextNode = doc.createTextNode(categoryText);
		Text weightTextNode = doc.createTextNode(weightText);
		
		luggage.appendChild(id);
		luggage.appendChild(identification);
		luggage.appendChild(category);
		luggage.appendChild(weight);
		id.appendChild(idTextNode);
		identification.appendChild(identificationTextNode);
		category.appendChild(categoryTextNode);
		weight.appendChild(weightTextNode);
		return luggage;
	}

	private Element createPassengerElement(Document doc) {
		Element passenger = doc.createElement("Passenger");
		Element reservationNumber = doc.createElement("ReservationNumber");
		Element firstName = doc.createElement("FirstName");
		Element lastName = doc.createElement("LastElement");
		
		Text reservationNumberTextNode = doc.createTextNode("CA937200305251");
		Text firstNameTextNode = doc.createTextNode("Anders");
		Text lastNameTextNode = doc.createTextNode("Munkholm");

		passenger.appendChild(reservationNumber);
		passenger.appendChild(firstName);
		passenger.appendChild(lastName);
		reservationNumber.appendChild(reservationNumberTextNode);
		firstName.appendChild(firstNameTextNode);
		lastName.appendChild(lastNameTextNode);
		
		return passenger;
	}

	private Element createFlightElement(Document doc) {
		Element flight = doc.createElement("Flight");
		Element origin = doc.createElement("Origin");
		Element destination = doc.createElement("Destination");
		Text originTextNode = doc.createTextNode("ARLANDA");
		Text destinationTextNode = doc.createTextNode("LONDON");
		
		
		flight.appendChild(origin);
		flight.appendChild(destination);
		origin.appendChild(originTextNode);
		destination.appendChild(destinationTextNode);
		return flight;
	}

	

}
