﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Messaging_system_server.InterpricePatterns
{
    //see the requester in messaging-system-client.InterpricePatterns for the other part of the pattern
    class Replier
    {

        private MessageQueue requestQueue;
        private MessageQueue errorQueue;

        public Replier(string requestQueueDestination,string errorQueueDestination)
        {
            this.requestQueue = CreateMessageQueue(requestQueueDestination);
            this.errorQueue = CreateMessageQueue(errorQueueDestination);

            requestQueue.MessageReadPropertyFilter.SetAll();
            ((XmlMessageFormatter)requestQueue.Formatter).TargetTypeNames = new string[] { "System.String,mscorlib" };
            //creates the reciever handler which handles the message that have been recieved
            requestQueue.ReceiveCompleted += new ReceiveCompletedEventHandler(OnRecievedMessage);

        }

        private void OnRecievedMessage(Object source, ReceiveCompletedEventArgs asyncResult)
        {
            MessageQueue requestQueue = (MessageQueue)source;
            //gets the message recieved
            Message requestMessage = requestQueue.EndReceive(asyncResult.AsyncResult);

            requestQueue.BeginReceive();

            try
            {
                string bodyRecieved = requestMessage.Body.ToString();
                Message replyMessage = CreateReplyMessage("oooookay..... but we don't like you", requestMessage);
                getReplyQueue(requestMessage).Send(replyMessage);


            } catch(Exception e)
            {
                requestMessage.CorrelationId = requestMessage.Id;

                errorQueue.Send(requestMessage);
            }
        }

        private MessageQueue getReplyQueue(Message requestMessage)
        {
            return requestMessage.ResponseQueue;
        }

        private Message CreateReplyMessage(string replyText, Message requestMessage)
        {
            Message message = new Message();
            message.CorrelationId = requestMessage.Id;
            return new Message(replyText);
        }



        private MessageQueue CreateMessageQueue(string destination)
        {
            try
            {
                return MessageQueue.Create(destination);
     
            }
            catch (Exception e)
            {
                return new MessageQueue(destination);
            }
        }
    }
}
