﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Messaging_system_client.InterpricePatterns
{
    //see messaging-system-server.InterpricePatterns replier for the other part of the pattern
    class Requester
    {
        private MessageQueue requestQueue;
        private MessageQueue replyQueue;
        private string messageText;
        


        public Requester(string requestQueueDestination, string replyQueueDestination, string messageText)
        {
            this.messageText = messageText;
            requestQueue = CreateMessageQueue(requestQueueDestination);
            replyQueue = CreateMessageQueue(replyQueueDestination);
            replyQueue.MessageReadPropertyFilter.SetAll();
            ((XmlMessageFormatter)replyQueue.Formatter).TargetTypeNames = new string[] { "System.String,mscorlib" };
        }

        public void Send()
        {
            Message message = new Message();
            message.Body = this.messageText;
            message.ResponseQueue = this.replyQueue;
            this.requestQueue.Send(message);
        }

        private MessageQueue CreateMessageQueue(string destination)
        {
            try
            {
                return MessageQueue.Create(destination);

            }
            catch (Exception e)
            {
                return new MessageQueue(destination);
            }
        }


        public void ReceiveSync()
        {
            Message replyMessage = replyQueue.Receive();

            Console.WriteLine("Received reply");
            Console.WriteLine("\tTime:       {0}", DateTime.Now.ToString("HH:mm:ss.ffffff"));
            Console.WriteLine("\tMessage ID: {0}", replyMessage.Id);
            Console.WriteLine("\tCorrel. ID: {0}", replyMessage.CorrelationId);
            Console.WriteLine("\tReply to:   {0}", "<n/a>");
            Console.WriteLine("\tContents:   {0}", replyMessage.Body.ToString());
        }
    }
}
