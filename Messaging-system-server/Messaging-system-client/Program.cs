﻿using Messaging_system_client.InterpricePatterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging_system_client
{
    class Program
    {
        static void Main(string[] args)
        {
            Requester requester = new Requester(@".\private$\RequestQueue", @".\private$\ReplyQueue", "we like you");
            requester.Send();
        }
    }
}
