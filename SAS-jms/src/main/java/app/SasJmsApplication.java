package app;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.model.Message;
import com.patterns.Subscriber;
import com.patterns.SubscriberTemplate;

import com.patterns.client.RequestReplyActions;
import com.patterns.client.SplitterActions;

import service.JmsMessageTemplateService;
@ComponentScan({"com.patterns.client","com.model","com.service", "com.repository", "com.patterns.client.listener"})
@SpringBootApplication
public class SasJmsApplication {
	

	static JmsMessageTemplateService jmsMessageTemplateService = new JmsMessageTemplateService();

	

	
	@Bean
    public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }
	
	@Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
	}
	
	
	
	

    public static void main(String[] args) {
        // Launch the application
        ConfigurableApplicationContext context = SpringApplication.run(SasJmsApplication.class, args);

        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
        jmsMessageTemplateService.saveConfigurableApplicationContext(context);

        // Send a message with a POJO - the template reuse the message converter
        System.out.println("Sending a message");
        
        
        jmsTemplate.convertAndSend("AirportInformationCenter", new Message("ScandinavianAirlineService", "message for the body field"));
        
        SplitterActions splitterActions = new SplitterActions();
        splitterActions.doSplitterActions();
        
        // send a message with request reply pattern being used
        RequestReplyActions requestReplyActions = new RequestReplyActions();
        requestReplyActions.doRequestReply();
       
        jmsTemplate.convertAndSend("AirportInformationCenterPublish", new Message("AirportInformationCenter SWA", "message for the body field"));
//        SubscriberTemplate subscriber = new SubscriberTemplate();
//        try {
//			subscriber.createSubsriber("publis2hAir", "AirportInformationCenterPublish");
//			
//		} catch (JMSException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        
    }

}
