# Welcome! #

This is a simple collection and implementation of enterprice patterns implemented in java spring
This project was made in relation to a course from erhvervsakadami aarhus in software development 


**CurrentPatterns implemented and running**

* MessageRouter
* Request-reply
* Publish-subscribe
* Splitter
* Resequencer(***can be implemented in many ways, to ensure the sequence is correct***)
* Aggregator(***can be implemented in many ways, to ensure the sequence is correct***)
* Transformer(***Time and date is strings so you have to do that part yourself if desired***)


*I might switch to c# at some point*


# Getting started #


Download the project from https://bitbucket.org/Anders_Munkholm/system-integration/downloads 
or create forks of the project.




# Getting started after downloading the project #



Install Spring tool suit from https://spring.io/tools/sts/all

Do the following

1.import projects-->maven-->existing maven projects-->browse->select the path where the project is located

2.wait for the projects to be initialized

  --if the project is not initialized by itself do the following

  --right click the project('s)-->maven-->update projects-->select the projects to be updated

3.open Boot Dashboard(a green button with a funny startup sign)

4.open the field named local

5.click on launch-airport-information-jms and start the server by clicking on the red button with a green triangle
  -Do this for all of the projects(except model service)

The project is now up and running




# Creating a new project #


1 . File-->new-->spring starter project-->next-->type jms into the search field-->select JMS(ActiveMQ)-->finish.

2 . open src/main/resources-->application.properties-->insert spring.activemq.broker-url= tcp://localhost:61616.

3 . open the pom.xml and insert this under <dependencies>

          
```
#!xml

<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
</dependency>
```


4 . insert this if you want to use a shared model/service across your projects. Just make sure all of the projects is inside of the same root folder



```
#!xml

<dependency>
   <groupId>com.model</groupId>
   <artifactId>model.jms</artifactId>
   <version>0.0.1-SNAPSHOT</version>
</dependency>
```




5 . copy paste the content of the main from sas or swa. You should now have a new application able to communicate with the JMS server set up by the information center appliciation

6 . make sure to add the correct Component scan(the package with the components class) under your SpringBootApplication, in order to use the components that you wish to include in your project. 

Here is an example Example


```
#!java

@SpringBootApplication
//@EnableJms only for the main project
@ComponentScan({"com.patterns.listener","com.model","com.service"})
public class MessagingSystemJmsApplication {
    ....
}

```



# Create a new connection address #


1 . Add this line of code to your main project's main class  and change the localhost address to something that you desire


```
#!java

@Bean(initMethod = "start", destroyMethod = "stop")
	public BrokerService broker() throws Exception {
		final BrokerService broker = new BrokerService();
		broker.addConnector("tcp://localhost:61616");
		broker.addConnector("vm://localhost");
		broker.setPersistent(false);
		return broker;
}
```

2 . Connecting to the address from your other projects is easy, simply add this line of code in the application.properties file. and make sure it matches the localhost address you just edited

```
#!properties


spring.activemq.broker-url= tcp://localhost:61616
```




## Important links ##

http://docs.spring.io/spring/docs/current/spring-framework-reference/html/jms.html