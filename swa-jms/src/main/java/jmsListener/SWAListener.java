package jmsListener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.model.Message;

@Component
public class SWAListener {

    @JmsListener(destination = "SouthWestAirlines", containerFactory = "myFactory")
    public void receiveMessageSAS(Message message) {
        System.out.println("SouthWestAirlines Received <" + message + ">");
    }

}
