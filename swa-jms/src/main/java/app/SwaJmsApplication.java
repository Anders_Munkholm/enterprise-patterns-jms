package app;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import com.model.Message;
import com.patterns.SubscriberTemplate;
import com.service.JmsMessageTemplateService;

@ComponentScan("jmsListener")
@SpringBootApplication
public class SwaJmsApplication {

	
	public void registerBeans(ConfigurableApplicationContext context ){
	    BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(JmsTemplate.class);
	    CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();

	    builder.addPropertyValue("connectionFactory", cachingConnectionFactory);      // set property value
	    DefaultListableBeanFactory factory = (DefaultListableBeanFactory) context.getAutowireCapableBeanFactory();
	    factory.registerBeanDefinition("jmsTemplateName", builder.getBeanDefinition());
	    
	}
	
	@Bean
	public JmsListenerContainerFactory<?> myTopicFactory(ConnectionFactory connectionFactory,
			DefaultJmsListenerContainerFactoryConfigurer configurer) {

		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setPubSubDomain(true);

		// This provides all boot's default to this factory, including the message converter
		configurer.configure(factory, connectionFactory);

		// You could still override some of Boot's default if necessary.
		return factory;
	}
	
	@Bean
    public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }
	
	@Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }
	
	
	

    public static void main(String[] args) {
        // Launch the application
        ConfigurableApplicationContext context = SpringApplication.run(SwaJmsApplication.class, args);
        JmsMessageTemplateService jmsMessageTemplateService = new JmsMessageTemplateService();
        jmsMessageTemplateService.saveConfigurableApplicationContext(context);
        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);

        
        // Send a message with a POJO - the template reuse the message converter
        System.out.println("Sending a message");
        
        jmsTemplate.convertAndSend("AirportInformationCenter", new Message("SouthWestAirlines", "message for the body field"));
        
        
        SubscriberTemplate subscriber = new SubscriberTemplate();
        try {
			subscriber.createSubsriber("publis1hAir", "AirportInformationCenterPublish");
			
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
